<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return redirect(url('/login'));
});

$router->group(['prefix' => 'api'], function () use ($router) {

            //AuthController
            $router->post('/auth/register', 'ApiController@register');
            $router->post('/auth/login', 'ApiController@login');
            $router->get('/auth/profile', 'ApiController@getProfile');
            $router->get('/auth/reset', 'ApiController@resetPassword');
            $router->post('/auth/update', 'ApiController@updateProfile');

            $router->get('/auth/get-transaksi', 'ApiController@getTransaksi');
            $router->get('/auth/history/', 'ApiController@historyById');
            $router->get('/auth/getAll', 'ApiController@getAll');
            
            // $router->get('/auth/getAdd', 'ApiController@getAdd');
            
            // //UserController
            $router->post('/auth/add-transaksi', 'ApiController@addTransaksi');
            $router->get('/user/{id_users}', 'ApiController@editProfile');
});

// admin
$router->group(
	[
		'middleware' => 'session', 'prefix' => 'admin'
	], 
	function () use ($router) {
	
	$router->get('/', 'AdminController@home');
	
	//management
	$router->get('/management', 'AdminController@managementUser');
	//tambah menegement
	
	$router->get('/management/add', 'AdminController@formManagementUserAdd');
	$router->post('/management/add', 'AdminController@managementUserAdd');

	//edit hapus manegement
	$router->get('/management/edit/{id_users}', 'AdminController@EditMangemenet');
	$router->post('/management/edit/{id_users}', 'AdminController@FormEditMangement');
	$router->get('/management/hapus/{id_users}', 'AdminController@hapusMangement');
	

	$router->get('/setting', 'AdminController@setting');
	$router->post('/setting', 'AdminController@formSetting');
	$router->get('/reporting', 'AdminController@reporting');
	$router->get('/reporting/cetak', 'AdminController@cetakReporting');
	$router->get('/cek', 'AdminController@test');

	
	//edit_master_grdae
	$router->get('grade-ikan/edit/grade/{id_grade}', 'MasterGradeIkan@gradeIkanEdit');
	$router->post('grade-ikan/edit/grade/{id_grade}', 'MasterGradeIkan@formEditGradeIkan');
	$router->get('grade-ikan/hapus/grade/{id_grade}', 'MasterGradeIkan@jenisIkanHapus');
	
	//jenis_ikan _master edit hapus 
	// $router->get('/hapus/jenis/{id_jenis_ikan}', 'MasterGradeIkan@jenisIkanHapus');
	$router->get('jenis-ikan/edit/jenis/{id_jenis_ikan}', 'MasterIkan@masterIkanEdit');
	$router->post('jenis-ikan/edit/jenis/{id_jenis_ikan}', 'MasterIkan@formIkanEdit');
	
	$router->get('jenis-ikan/hapus/jenis/{id_jenis_ikan}', 'MasterIkan@jenisIkanHapus');

	// jenis ikan and grade
	$router->get('/jenis-ikan', 'MasterIkan@getAll');
	$router->get('/grade-ikan', 'MasterGradeIkan@getAll');

	
	//master ikan
	$router->get('/jenis-ikan/add', 'MasterIkan@jenisIkan');
	$router->post('/jenis-ikan/add', 'MasterIkan@formJenisIkan');
	
	//master grade
	$router->get('/grade-ikan/add', 'MasterGradeIkan@gradeIKan');
	$router->post('/grade-ikan/add', 'MasterGradeIkan@formgradeIKan');

	//logout admin
	$router->get('/logout-admin', 'AdminController@logoutAdmin');

	$router->get('/file/{name}', 'AdminController@getFile');;
});

// user
$router->group(['prefix' => 'user'], function () use ($router){

	$router->get('/', 'UserController@userHome');
	$router->get('/setting', 'UserController@setting');
	$router->post('/setting', 'UserController@settingForm');
	$router->get('/reporting', 'UserController@reporting');
	$router->get('/logout-user', 'UserController@logoutUser');
});

// auth
$router->group([], function () use ($router) {

	$router->get('/login', 'AuthController@login');
	$router->post('/login', 'AuthController@loginForm');
    
    $router->get('/register', 'AuthController@register');
	$router->post('/register', 'AuthController@registerForm');
	
	$router->get('/forgot-password', 'AuthController@forgotPassword');

	$router->get('/logout', 'AuthController@logout');
});

// file
$router->get('/file/{name}', 'AuthController@getFile');