<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    protected $table = 'nelayan_users';

}

