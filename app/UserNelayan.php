<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNelayan extends Model
{
    protected $table = 'nelayan_users';
    protected $primaryKey = 'id_users';

    public function user_profile()
    {
    	return $this->hasMany('App\UserProfile');
    }
}
