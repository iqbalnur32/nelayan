<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'nelayan_profiles';
     protected $primaryKey = 'id_profile';

    public function user_nelayan()
    {
    	return $this->belongsTo('App\UserNelayan');
    }
}	
