<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MasterGrade as MasterGrade;
use App\MasterIkan;

class MasterGradeIkan extends controller{
	
	private $request;

	public function __construct(Request $request) {
		$this->request = $request;
	}

	// Master Grade Ikan
	public function gradeIKan()
	{
		return view('src/admin/master/master_grade/tambah_grade');
	}

	// Fungsi Get All
	public function getAll()
	{
		$data_ikan = MasterGrade::all();
		// print_r($data_ikan);
		// die();
		return view('src/admin/master/master_grade/jenis-grade', ['data_ikan' => $data_ikan]);
	}

	// Fungsi Grade Hapus
	public function jenisIkanHapus($id_grade)
	{
		$hapus_grade = MasterGrade::find($id_grade);
		$hapus_grade->delete();
		return redirect(url('/admin/grade-ikan'));
	}

	// Fungsi Edit Grade
	public function gradeIkanEdit($id_grade)
	{
		// $grade_ikan = MasterGrade::find($id_grade)->first();
		$grade_ikan = MasterGrade::select(['*'])
		->where('master_grade.id_grade', $id_grade)
		->first();
		return view('src/admin/master/master_grade/edit', ['grade_ikan' => $grade_ikan]);
	}

	// Form Post Edit Ikan
	public function formEditGradeIkan($id_grade, Request $request)
	{
		$this->validate($request, [
			'name' => 'required|string',
			'description' => 'required|string',
			'berat_min' => 'required|string',
			'berat_max' => 'required|string',
		]);
		try{
			$edit = MasterGrade::find($id_grade);
			$edit->name = $request->input('name');
			$edit->description = $request->input('description');
			$edit->berat_min = $request->input('berat_min');
			$edit->berat_max = $request->input('berat_max');

			$data = $edit->update();

			if($data){
				return redirect(url('admin/grade-ikan'));
			}
		}catch (\Exception $e){
			return view('src/admin/master/master_grade/edit');
		}

	}

	// Form Grade Ikan
	public function formgradeIKan(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|string',
			'description' => 'required|string',
			'berat_min' => 'required|string',
			'berat_max' => 'required|string',
		]);

		try{
			$id_user = MasterGrade::select('id_jenis_ikan')->where('name', $request->input('name'))->first();

			$master = new MasterGrade;
			$master->name = $request->input('name');
			$master->id_jenis_ikan = 44;
			$master->description = $request->input('description');
			$master->berat_min = $request->input('berat_min');
			$master->berat_max = $request->input('berat_max');

			$array_success = [
				'type' => 'success',
				"title" => 'Success !',
				'msg' => 'Berhasil Menambahkan Jenis Ikan'
			];
			
			$data = $master->save();
			if($data){
				return view('src/admin/master/master_grade/tambah_grade', $array_success);
			}


		} catch (\Exception $e) {
            //return error message
           // return view('src/admin/tambah_grade');
		   return view('src/admin/master/master_grade/tambah_grade');
		}	
	}


}


?>