<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\UserNelayan;
use App\UserProfile as Profile;
use App\NelayanLevel as level;

class AuthController extends Controller
{
	private $request;

	//session pada user dan admin
	public function __construct(Request $request) {
        $this->request = $request;
		session_start();
    }
	
    // login form
	public function login()
	{
		return view('src/auth/login');

	}

	// post login form
	public function loginForm(Request $request)
	{
	
		$this->validate($request, [
			'email' => 'required|email',
			'password' => 'required|string',
		]);

		$failed_alert = [
			'type' => 'error',
			"title" => 'error !',
			'msg' => 'Gagal Login'
		];
		
		try{
			$email = $request->input('email');
			$password = sha1($request->input('password'));
			
			//email wehre
			$where = [
				"email" => $email,
				"password" => $password
			];

			$data = UserNelayan::where($where)->first();

			if($data){

				$profile = Profile::where('id_profile', $data->id_users)->first();
				
				$dataSession = [
					'user_id' => $data->id_users,
					'level_id' => $data->level_id, 
					'nama' => $profile->name,
					'no_telp' => $profile->no_telp,
					'foto' => $profile->foto
				];

				// save session
				$_SESSION = $dataSession;

				if($data->level_id === 1){
					
					return redirect(url('/admin'));
				
				}else if($data->level_id === 2){
					
					return redirect(url('/user'));
			}else{
				return view('src/auth/login', $failed_alert);
				}
			}else{
				 return view('/src/auth/login', $failed_alert);
			}

		}catch(\Exception $e){
			 return view('/src/auth/login', $failed_alert);
		}
	}

	// Fungsi Get Register
	public function register()
	{		
		$level = Level::all();
		return view('src/auth/register', ['level' => $level]);
		// return view('src/auth/register');
	}

	private function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	// Fungsi Post Register Form
	public function registerForm(Request $request)
	{
		$avatar = $this->generateRandomString(35);
		$upload = $request->file('foto')->move(storage_path('image'), $avatar);

		$this->validate($request, [
			'nama' => 'required|string',
			'password' => 'required|confirmed',
			'username' => 'required|string',
			'email' => 'required|string|',
			'alamat' => 'required|string',
			'no_telp' => 'required|string',
			
		]);
		
		try {
			
			// insert data nelayan_users
			$insert = new UserNelayan;
			$insert->username = $request->input('username');
			$insert->email = $request->input('email');
			$insert->password = sha1($request->input('password'));
			$insert->level_id = 3;

			$save = $insert->save();

			if($save){

				$id_user = UserNelayan::select('id_users')->where('email', $request->input('email'))->first();
				
				// insert data nealayan_profiles
				$insert2 = new Profile;
				$insert2->id_profile = $id_user->id_users;
				$insert2->name = $request->input('nama');
				$insert2->alamat = $request->input('alamat');
				$insert2->no_telp = $request->input('no_telp');
				$insert2->foto = $avatar;
				$save2 = $insert2->save();

				if($save2){
					return redirect(url('login'));
					// echo 'berhasil';
				}else{
					UserNelayan::where('email',$request->input('email'))->delete();
					return view('src/auth/register');
				}
			}else{
				return view('src/auth/register');
			}
		} catch (Exception $e) {
			echo $e;
		}	
	}

	// Fungsi Forgot Password
	public function forgotPassword()
	{
		return view('src/auth/forgot_password');
	}


	/*
		Auth Logout
	*/
	public function logout()
	{
		if(session_destroy()){
			return redirect(url('/login'));
		}
	}

	/* Get File */
	public function getFile($name)
	{
		$avatar_path = storage_path('image') . '/' . $name;
		
		if (file_exists($avatar_path)) {
		    $file = file_get_contents($avatar_path);
		    return response($file, 200)->header('Content-Type', 'image/jpeg');
	    }

	    return "File Not Found";
	}
}	

/*
	Admin = 1
	Users = 2
	Nelayan = 3
*/