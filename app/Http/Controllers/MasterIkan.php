<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\MasterGrade as MasterGrade;
use App\MasterIkan as MasterJenis;

class MasterIkan extends Controller{
	
	private $request;

	public function __construct(Request $request) {
		$this->request = $request;
	}

	// Get All Master Jenis Ikan
	public function getAll()
	{
		
		$data = MasterJenis::all();
		return view('/src/admin/master/jenis_ikan/jenis_ikan', ['data' => $data]);
		
	}

	// Form Get Jenis Ikan 
	public function jenisIkan()
	{
		return view('src/admin/master/jenis_ikan/tambah_ikan');
	}

	// Form Post Get Ikan
	public function formJenisIkan(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|string',
			'description' => 'required|string',
		]);

		try{
			$master = new MasterJenis;
			$master->name = $request->input('name');
			$master->description = $request->input('description');
			$data = $master->save();

			if($data){

				$array = [
					'type' => 'success',
					"title" => 'Success !',
					'msg' => 'Berhasil Menambahkan Jenis Ikan'
				];
				return view('src/admin/master/jenis_ikan/tambah_ikan', $array);
			}
			
		} catch (\Exception $e) {
            //return error message
			return redirect(url('admin/jenis-ikan'));
		}	
	}	

	// Fungsi Edit Master Ikan
	public function masterIkanEdit($id_jenis_ikan)
	{
		// $data_edit = MasterJenis::find($id_jenis_ikan)->first();
		$data_edit = MasterJenis::select(['*'])
		->where('master_jenis_ikan.id_jenis_ikan', $id_jenis_ikan)
		->first();
		// print_r($data_edit);
		// die();
		return view('src/admin/master/jenis_ikan/edit', ['data_edit' => $data_edit]);
	}

	// Fungsi Edit Ikan
	public function formIkanEdit(Request $request, $id_jenis_ikan)
	{
		$this->validate($request, [
			'name' => 'required|string',
			'description' => 'required|string',
		]);

		try{
			$master = MasterJenis::find($id_jenis_ikan);
			$master->name = $request->input('name');
			$master->description = $request->input('description');
			$dataEdit = $master->update();

			if($dataEdit){
				return redirect(url('admin/jenis-ikan'));
			}else{
				return view('src/admin/master/master_jenis/tambah_ikan');
			}
		}catch(\Exception $e){
			return view('src/admin/master/master_jenis/tambah_ikan');
		}
	}

	// Fungsi Ikan Hapus
	public function jenisIkanHapus($id_jenis_ikan)
	{
		$hapus_jenis = MasterJenis::find($id_jenis_ikan);
		$hapus_jenis->delete();
		return redirect(url('/admin/jenis-ikan'));
	}
}


?>