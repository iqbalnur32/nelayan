<?php
    namespace App\Http\Controllers;

    use DB;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Auth;
    use App\UserNelayan;
    use App\UserProfile as Profile;
    use App\NelayanLevel as Level;
    use App\MasterIkan;
    use App\MasterGrade;
    use App\NelayanTpi;
    use App\NelayanSubTpi;
    use App\NelayanTransaksi;
    use Firebase\JWT\JWT;

    class ApiController extends Controller{

     private $request;
    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        
    }

    // Response Success
    private function resSuccess($code, $status, $data)
    {
        return response()->json([
            'code' => $code, 'status' => $status, 'data' => $data
        ]);
    }

    // Response Error 
    private function resError($code, $status, $msg)
    {
        return response()->json([
            'code' => $code, 'status' => $status, 'msg' => $msg
        ]);
    }

    // Fungsi Jwt
    private function jwt($user)
    {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user, // Subject of the token
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    // Fungsi Register
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'username' => 'required|string',
            'email' => 'required|string|email|unique:nelayan_users',
            'alamat' => 'required|string',
            'no_telp' => 'required|string',
            
        ]);
        
        try {
            // insert data nelayan_users
            $insert = new UserNelayan;
            $insert->username = $request->input('username');
            $insert->email = $request->input('email');
            $insert->password = sha1($request->input('password'));
            $insert->level_id = $request->input('level_id');

            $save = $insert->save();

            if($save){

                $id_user = UserNelayan::select('id_users')->where('email', $request->input('email'))->first();
                
                // insert data nealayan_profiles
                $insert2 = new Profile;
                $insert2->id_profile = $id_user->id_users;
                $insert2->name = $request->input('name');
                $insert2->alamat = $request->input('alamat');
                $insert2->no_telp = $request->input('no_telp');
                $insert2->foto = "https://img.jpg/sdsdsldk";
                $save2 = $insert2->save();

                if($save2){
                  return $this->resSuccess(200, 'success', [
                    'user' => $save2,
                  ]);
                    // echo 'berhasil';
                }else{
                    UserNelayan::where('email',$request->input('email'))->delete();
                    return $this->resError(401, 'error', 'Register Failed');
                }
            }else{
                UserNelayan::where('email',$request->input('email'))->delete();
                 return $this->resError(401, 'error', 'Register Failed');
            }
        } catch (Exception $e) {
              return $this->resError(500, 'error', 'Invalid request');
        }
    }


    public function login(Request $request){
        //validate incoming request 
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|string',
        ]);
        
        try{
            $email = $request->input('email');
            $password = sha1($request->input('password'));
            
            //email wehre
            $where = [
                "email" => $email,
                "password" => $password
            ];

            $data = UserNelayan::where($where)->first();

            if($data){

                $profile = Profile::where('id_profile', $data->id_users)->first();

                if($data->level_id === 3){

                    return $this->resSuccess(200, 'success', [
                        'user' => $profile,
                        'token' => $this->jwt($profile)
                    ]);

                }else{
                return $this->resError(401, 'error', 'User Not Found');   
                }
            }else{
               return $this->resError(401, 'error', 'Request Data Not Valid');
           }

       }catch(\Exception $e){
            return $this->resError(500, 'error', 'Invalid Request');
       }
    }

    // Reset Password
    public function resetPassword(Request $request)
    {
        return response()->json(['user' => UserNelayan::all()], 200);
    }

    // Get Transaksi
    public function getTransaksi()
    {
       $nelayan_tpi = NelayanSubTpi::select(['*', 'nelayan_sub_tpi.created_at as tanggal'])
        ->join('nelayan_tpi', 'nelayan_sub_tpi.id_sub_tpi', '=', 'nelayan_tpi.id_tpi')
        ->join('nelayan_transaksi', 'nelayan_sub_tpi.id_tpi', '=', 'nelayan_transaksi.id_transaksi')
        ->get(); 

        echo $nelayan_tpi;
    }   

    // Add Transaksi
    public function AddTransaksi(Request $request)
    {   

        $insert = new NelayanTransaksi;
        $insert->tpi_id = $request->input('tpi');
        $insert->tpi_sub_id = $request->input('tpi_sub_id');
        $insert->user_id = $request->input('user_id');
        $insert->jenis_id = $request->input('jenis_id');
        $insert->grade_id = $request->input('grade_ikan');
        $insert->berat_satuan = $request->input('berat_satuan');
        $insert->berat_total = $request->input('berat_total');
        $insert->harga_kg = $request->input('harga_kg');
        $insert->foto = $request->input('foto');

        $save = $insert->save();
        if($save){
            return $this->resSuccess(200, 'success', [
                'user' => $save
            ]);
        }else{
            return $this->resError(200, 'error', 'Failed Add Transaksi');
        }
    }

    // History Data By uSer id
    public function historyById(Request $request)
    {      
        // $find = NelayanTransaksi::find($tpi_id);
        // echo $find;
        // die();
        $history = NelayanTransaksi::where('created_at', 'like', '%' . $request->input('tanggal') . '%')
                ->get();

        echo $history;
    }

    // Get All Master Ikan
    public function getAll()
    {   
        $all = MasterGrade::all();
        // $data_user = MasterIkan::select(['*', 'master_jenis_ikan.*'])
        // ->join('master_grade', 'master_jenis_ikan.id_jenis_ikan', '=', 'master_grade.id_grade')
        // ->get();
        $data_user = MasterIkan::all();
        return $this->resSuccess(200, 'success', [
            'user' => $data_user,
            'user' => $all
        ]);
        
    }

    //  Get Profile
    public function getProfile($id_users)
    {
        $data_edit = UserNelayan::select(['*', 'nelayan_users.created_at as tanggal'])
        ->join('nelayan_profiles', 'nelayan_users.id_users', '=', 'nelayan_profiles.id_profile')
        ->join('nelayan_level', 'nelayan_users.level_id', '=', 'nelayan_level.id_level')
        ->where('nelayan_users.id_users', $id_users)
        ->first();

        if($data_edit){
            return $this->resSuccess(200, 'success', [
                'user'=> $data_edit,
            ]);
        }else{
            return $this->resError(401, 'gagal', 'User Tidak Ditemukan');
        }
        
    }

    // Update Profile
    public function updateProfile(Request $request)
    {
        $this->validate($request, [
			// 'name' => 'required|string',
			'id_users' => 'required',
			'username' => 'required|string',
			'email' => 'required|string|email|',
			'alamat' => 'required|string',
			'no_telp' => 'required|string',
			
		]);
		
		try {	
			// insert data nelayan_users
			$insert = UserNelayan::find($request->input('id_users'));
			$insert->username = $request->input('username');
			$insert->email = $request->input('email');
			$insert->password = sha1($request->input('password'));
			$insert->level_id = $request->input('level_id');

			$save = $insert->update();

			if($save){

				$id_user = UserNelayan::select('id_users')->where('email', $request->input('email'))->first();
				
				// insert data nealayan_profiles
				$insert2 = Profile::find($request->input('id_users'));
				$insert2->id_profile = $id_user->id_users;
				$insert2->name = $request->input('name');
				$insert2->alamat = $request->input('alamat');
				$insert2->no_telp = $request->input('no_telp');
				$insert2->foto = 'https://img.jpg/sdsdsldk';
				$save2 = $insert2->update();

				if($save2){
					return $this->resSuccess(200, 'success', [
                        'user' => $save2,
                    ]);
					// echo 'berhasil';
				}else{
					UserNelayan::where('email',$request->input('email'))->delete();
					return $this->resError(401, 'success');
				}
			}else{
				return $this->resError(401, 'success');
			}
		} catch (Exception $e) {

		}
    }


}