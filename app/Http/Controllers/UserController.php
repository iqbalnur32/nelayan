<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserProfile;
use App\UserNelayan;
use App\NelayanLevel as Level;

class UserController extends Controller
{
	public function __construct(Request $request) {
		$this->request = $request;
		session_start();
	}

	// Dashboar User Home
	public function userHome()
	{
		return view('src/users/home');
	}

	// Fungsi Setting All
	public function setting()
	{	
		$level = level::all();
		$data_edit = UserNelayan::select(['*', 'nelayan_users.created_at as tanggal'])
		->join('nelayan_profiles', 'nelayan_users.id_users', '=', 'nelayan_profiles.id_profile')
		->join('nelayan_level', 'nelayan_users.level_id', '=', 'nelayan_level.id_level')
		->where('nelayan_users.id_users', $_SESSION['user_id'])
		->first();

		return view('src/users/setting', ['data_edit' => $data_edit, 'level' => $level]);
	}

	// Fungsi Post Settung Form
	public function settingForm(Request $request)
	{
		$avatar = $this->generateRandomString(35);
		$upload = $request->file('foto')->move(storage_path('image'), $avatar);

		$this->validate($request, [
			// 'name' => 'required|string',
			'id_users' => 'required',
			'username' => 'required|string',
			'email' => 'required|string|email|',
			'alamat' => 'required|string',
			'no_telp' => 'required|string',
			
		]);
		
		try {	
			// insert data nelayan_users
			$insert = UserNelayan::find($request->input('id_users'));
			$insert->username = $request->input('username');
			$insert->email = $request->input('email');
			$insert->password = sha1($request->input('password'));
			$insert->level_id = $request->input('level_id');

			$save = $insert->update();

			if($save){

				$id_user = UserNelayan::select('id_users')->where('email', $request->input('email'))->first();
				
				// insert data nealayan_profiles
				$insert2 = UserProfile::find($request->input('id_users'));
				$insert2->id_profile = $id_user->id_users;
				$insert2->name = $request->input('nama');
				$insert2->alamat = $request->input('alamat');
				$insert2->no_telp = $request->input('no_telp');
				$insert2->foto = $avatar;
				$save2 = $insert2->update();

				if($save2){
					return redirect(url('user/setting'));
					// echo 'berhasil';
				}else{
					UserNelayan::where('email',$request->input('email'))->delete();
					return view('src/users/setting');
				}
			}else{
				return view('src/users/setting');
			}
		} catch (Exception $e) {
			return view('/src/users/setting');
		}
	}

	// Fungsi Reporting
	public function reporting()
	{
		return view('src/users/reporting');
	}

	// Fungsi Logout User
	public function logoutUser()
	{
		# code...
		if(session_destroy()){
			return redirect(url('login'));
		}
	}
}
?>