<?php

namespace App\Http\Controllers;

use DB;
// use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\UserProfile;
use App\UserNelayan;
use App\NelayanLevel as Level;
use App\MasterGrade;
use App\MasterIkan;
//use App\UserProfile as Profile;


class AdminController extends Controller
{
	public function __construct(Request $request) {
		$this->request = $request;
	}

	//dashboard admin
	public function home()
	{
		return view('src/admin/home');
	}	

	//GetAll Table Manengement User
	public function managementUser(Request $request)
	{
		$data_user = UserNelayan::select(['*', 'nelayan_users.created_at as tanggal'])
		->join('nelayan_profiles', 'nelayan_users.id_users', '=', 'nelayan_profiles.id_profile')
        ->join('nelayan_level', 'nelayan_users.level_id', '=', 'nelayan_level.id_level')
        ->get(); 
        // echo json_encode($data_user);
        // die();

       	return view('src/admin/manegement/manegement', ['data_user' => $data_user]);
	}

	//Form Add User Mangement
	public function formManagementUserAdd()
	{
		$level = Level::all();
		return view('src/admin/manegement/tambah_user', ['level' => $level]);
	}

	//Generate Random
	private function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	// Add Form User Management
	public function managementUserAdd(Request $request)
	{
		$avatar = $this->generateRandomString(35);
		$upload = $request->file('foto')->move(storage_path('app'), $avatar);

		$this->validate($request, [
			// 'name' => 'required|string',
			'username' => 'required|string',
			'email' => 'required|string|email|unique:nelayan_users',
			'alamat' => 'required|string',
			'no_telp' => 'required|string',
			
		]);
		
		try {
			
			// insert data nelayan_users
			$insert = new UserNelayan;
			$insert->username = $request->input('username');
			$insert->email = $request->input('email');
			$insert->password = sha1($request->input('password'));
			$insert->level_id = $request->input('level_id');

			$save = $insert->save();

			if($save){

				$id_user = UserNelayan::select('id_users')->where('email', $request->input('email'))->first();
				
				// insert data nealayan_profiles
				$insert2 = new UserProfile;
				$insert2->id_profile = $id_user->id_users;
				$insert2->name = $request->input('nama');
				$insert2->alamat = $request->input('alamat');
				$insert2->no_telp = $request->input('no_telp');
				$insert2->foto = $avatar;
				$save2 = $insert2->save();

				if($save2){
				  return redirect(url('admin/management'));
					// echo 'berhasil';
				}else{
					UserNelayan::where('email',$request->input('email'))->delete();
					return view('src/admin/manegement/tambah_user');
				}
			}else{
				UserNelayan::where('email',$request->input('email'))->delete();
				return view('src/admin/manegement/tambah_user');
			}
		} catch (Exception $e) {
				
		}
	}

	// Edit User Mangement
	public function EditMangemenet($id_users)
	{
		$level = Level::all();
		$data_edit = UserNelayan::select(['*', 'nelayan_users.created_at as tanggal'])
		->join('nelayan_profiles', 'nelayan_users.id_users', '=', 'nelayan_profiles.id_profile')
		->join('nelayan_level', 'nelayan_users.level_id', '=', 'nelayan_level.id_level')
		->where('nelayan_users.id_users', $id_users)
		->first();

		return view('src/admin/manegement/edit', ['data_edit' => $data_edit, 'level' => $level]);
		
	}

	// FormEditMangement
	public function FormEditMangement(Request $request)
	{
		$avatar = $this->generateRandomString(35);
		$upload = $request->file('foto')->move(storage_path('image'), $avatar);

		$this->validate($request, [
			// 'name' => 'required|string',
			'id_users' => 'required',
			'username' => 'required|string',
			'email' => 'required|string|email|',
			'alamat' => 'required|string',
			'no_telp' => 'required|string',
			
		]);
		
		try {	
			// insert data nelayan_users
			$insert = UserNelayan::find($request->input('id_users'));
			$insert->username = $request->input('username');
			$insert->email = $request->input('email');
			$insert->password = sha1($request->input('password'));
			$insert->level_id = $request->input('level_id');

			$save = $insert->update();

			if($save){

				$id_user = UserNelayan::select('id_users')->where('email', $request->input('email'))->first();
				
				// insert data nealayan_profiles
				$insert2 = UserProfile::find($request->input('id_users'));
				$insert2->id_profile = $id_user->id_users;
				$insert2->name = $request->input('nama');
				$insert2->alamat = $request->input('alamat');
				$insert2->no_telp = $request->input('no_telp');
				$insert2->foto = $avatar;
				$save2 = $insert2->update();

				if($save2){
					return redirect(url('admin/management'));
					// echo 'berhasil';
				}else{
					UserNelayan::where('email',$request->input('email'))->delete();
					return view('src/admin/manegement/edit');
				}
			}else{
				return view('src/admin/manegement/edit');
			}
		} catch (Exception $e) {

		}
	}

	// Delete User Mengement
	public function hapusMangement($id_users)
	{
		$hapus_profile = UserProfile::find($id_users)->delete();
		$hapus_user = UserNelayan::find($id_users)->delete();
		
		// if($hapus_profile && $hapus_user){
		// 	echo "data terhapus";
		// }else{
		// 	echo "data gagal";
		// }
		// die();
		
		return redirect(url('admin/management'));
	}

	//Menu Setting
	public function setting()
	{	
		$level = level::all();
		$data_edit = UserNelayan::select(['*', 'nelayan_users.created_at as tanggal'])
		->join('nelayan_profiles', 'nelayan_users.id_users', '=', 'nelayan_profiles.id_profile')
		->join('nelayan_level', 'nelayan_users.level_id', '=', 'nelayan_level.id_level')
		->where('nelayan_users.id_users', $_SESSION['user_id'])
		->first();

		return view('src/admin/setting', ['data_edit' => $data_edit, 'level' => $level]);
		// return view('src/admin/setting', ['data_setting' => $data_setting]);
	}

	//Menu Reset Password
	public function formSetting(Request $request)
	{
		$avatar = $this->generateRandomString(35);
		$upload = $request->file('foto')->move(storage_path('image'), $avatar);

		$this->validate($request, [
			// 'name' => 'required|string',
			'id_users' => 'required',
			'username' => 'required|string',
			'email' => 'required|string|email|',
			'alamat' => 'required|string',
			'no_telp' => 'required|string',
			
		]);
		
		try {	
			// insert data nelayan_users
			$insert = UserNelayan::find($request->input('id_users'));
			$insert->username = $request->input('username');
			$insert->email = $request->input('email');
			$insert->password = sha1($request->input('password'));
			$insert->level_id = $request->input('level_id');

			$save = $insert->update();

			if($save){

				$id_user = UserNelayan::select('id_users')->where('email', $request->input('email'))->first();
				
				// insert data nealayan_profiles
				$insert2 = UserProfile::find($request->input('id_users'));
				$insert2->id_profile = $id_user->id_users;
				$insert2->name = $request->input('nama');
				$insert2->alamat = $request->input('alamat');
				$insert2->no_telp = $request->input('no_telp');
				$insert2->foto = $avatar;
				$save2 = $insert2->update();

				if($save2){
					return redirect(url('admin/setting'));
					// echo 'berhasil';
				}else{
					UserNelayan::where('email',$request->input('email'))->delete();
					return view('src/admin/setting');
				}
			}else{
				return view('src/admin/setting');
			}
		} catch (Exception $e) {
			return view('/src/admin/setting');
		}
	}	

	// Reporting
	public function reporting()
	{	
		// $data_reporting = DB::table('master_jenis_ikan')->join('master_grade','master_jenis_ikan.id_jenis_ikan','=','master_grade.id_jenis_ikan')->get();
        // echo json_encode($data_reporting);
        // die();

        // echo json_encode($data_reporting);
        // die();

       	return view('src/admin/reporting');
			
	}

	

	public function test()
	{
		/*$data_user = MasterGrade::select(['master_jenis_ikan.*', 'master_grade.*'])
		->join('master_jenis_ikan', 'master_jenis_ikan.id_jenis_ikan', '=', 'master_grade.id_grade')
		->get();*/
		/*$data_user = DB::select('*')->from('master_jenis_ikan')->join('master_grade','master_grade.id_jenis_ikan = master_jenis_ikan.id_jenis_ikan')->get();*/
		$data_user = DB::table('master_grade')
		->join('master_jenis_ikan','master_grade.id_jenis_ikan','=','master_jenis_ikan.id_jenis_ikan')
		->get();
  
		print_r($data_user);
	}

	// Logout Admin
	public function logoutAdmin()
	{
		# code...
		if(session_destroy()){
			return redirect(url('login'));
		}
	}

	public function getFile($name)
	{
		$avatar_path = storage_path('image') . '/' . $name;
		
		if (file_exists($avatar_path)) {
			$file = file_get_contents($avatar_path);
			return response($file, 200)->header('Content-Type', 'image/jpeg');
		}

		return "File Not Found";
	}
}
