<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NelayanLevel extends Model
{
    protected $table = 'nelayan_level';	
    protected $primaryKey = 'id_level';

}	
