<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NelayanSubTpiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nelayan_sub_tpi', function (Blueprint $table) {
            $table->increments('id_sub_tpi');
            $table->integer('id_tpi');
            $table->integer('provinsi_id');
            $table->integer('kota_id');
            $table->string('alamat', 255);
            $table->string('kode_pos', 10);
            $table->string('nama_sub_tpi', 45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nelayan_sub_tpi');
    }
}
