<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NelayanTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nelayan_transaksi', function (Blueprint $table) {
            $table->increments('id_transaksi');
            $table->integer('tpi_id');
            $table->integer('tpi_sub_id');
            $table->integer('user_id');
            $table->integer('jenis_id');
            $table->integer('grade_id');
            $table->string('berat_satuan', 5);
            $table->string('berat_total', 5);
            $table->string('harga_kg',  25);
            $table->string('foto', 225);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nelayan_transaksi');
    }
}
