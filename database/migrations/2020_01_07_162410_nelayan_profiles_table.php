<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NelayanProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nelayan_profiles', function (Blueprint $table) {
            $table->integer('id_profile');
            $table->string('name', 45);
            $table->string('alamat', 45);
            $table->string('no_telp', 45);
            $table->string('foto', 45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nelayan_profiles');
    }
}
