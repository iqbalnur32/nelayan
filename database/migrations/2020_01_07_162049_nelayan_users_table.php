<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NelayanUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nelayan_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_users');
            $table->string('username', 35);
            $table->string('password', 45);
            $table->string('email', 75);
            $table->timestamps();
            $table->integer('level_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nelayan_users');
    }
}
