<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterGradeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_grade', function (Blueprint $table) {
            $table->integer('id_grade')->PRIMARY_KEY();
            $table->integer('jenis_ikan');
            $table->string('name', 45);
            $table->string('description', 125);
            $table->string('berat_min',10);
            $table->string('berat_max', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_grade');
    }
}
