-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Jan 2020 pada 08.52
-- Versi server: 10.1.40-MariaDB
-- Versi PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nelayan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_grade`
--

CREATE TABLE `master_grade` (
  `id_grade` int(11) NOT NULL,
  `jenis_ikan` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `berat_min` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `berat_max` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `master_grade`
--

INSERT INTO `master_grade` (`id_grade`, `jenis_ikan`, `name`, `description`, `berat_min`, `berat_max`, `created_at`, `updated_at`) VALUES
(1, 0, 'kakap-putih', 'ikan kakap', '2kg', '5kg', '2020-01-08 18:56:47', '2020-01-08 18:56:47'),
(2, 0, 'mujair', 'ikan mujair', '4kg', '5kg', '2020-01-08 18:56:47', '2020-01-08 18:56:47'),
(3, 0, 'tongkol', 'ikan tongkol', '3kg', '7kg', '2020-01-08 18:56:47', '2020-01-08 18:56:47'),
(4, 0, 'kerapu', 'ikan kerapu', '4kg', '9kg', '2020-01-08 18:56:47', '2020-01-08 18:56:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_jenis_ikan`
--

CREATE TABLE `master_jenis_ikan` (
  `id_jenis_ikan` int(10) NOT NULL,
  `name` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `master_jenis_ikan`
--

INSERT INTO `master_jenis_ikan` (`id_jenis_ikan`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'kakap', 'kakap-putih', '2020-01-08 18:58:47', '2020-01-08 18:58:47'),
(2, 'mujair', 'mujair-merah', '2020-01-08 18:58:47', '2020-01-08 18:58:47'),
(3, 'tongkol', 'tongkol', '2020-01-08 18:58:47', '2020-01-08 18:58:47'),
(4, 'kerapu', 'kerapu', '2020-01-08 18:58:47', '2020-01-08 18:58:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_01_07_162049_nelayan_users_table', 1),
(2, '2020_01_07_162410_nelayan_profiles_table', 1),
(3, '2020_01_07_162415_nelayan_level_table', 1),
(4, '2020_01_07_165158_master_grade_table', 1),
(5, '2020_01_07_165446_master_jenis_ikan_table', 1),
(6, '2020_01_07_182712_nelayan_transaksi_table', 1),
(7, '2020_01_07_183059_nelayan_tpi_table', 1),
(8, '2020_01_07_183400_nelayan_sub_tpi_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nelayan_level`
--

CREATE TABLE `nelayan_level` (
  `id_level` int(10) UNSIGNED NOT NULL,
  `name` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `nelayan_level`
--

INSERT INTO `nelayan_level` (`id_level`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'user', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nelayan_profiles`
--

CREATE TABLE `nelayan_profiles` (
  `id_profile` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nelayan_sub_tpi`
--

CREATE TABLE `nelayan_sub_tpi` (
  `id_sub_tpi` int(10) NOT NULL,
  `id_tpi` int(11) NOT NULL,
  `provinsi_id` int(11) NOT NULL,
  `kota_id` int(11) NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_pos` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_sub_tpi` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nelayan_tpi`
--

CREATE TABLE `nelayan_tpi` (
  `id_tpi` int(10) NOT NULL,
  `nama_tpi` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nelayan_transaksi`
--

CREATE TABLE `nelayan_transaksi` (
  `id_transaksi` int(10) UNSIGNED NOT NULL,
  `tpi_id` int(11) NOT NULL,
  `tpi_sub_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `jenis_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `berat_satuan` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `berat_total` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_kg` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nelayan_users`
--

CREATE TABLE `nelayan_users` (
  `id_users` int(11) NOT NULL,
  `username` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `level_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `nelayan_users`
--

INSERT INTO `nelayan_users` (`id_users`, `username`, `password`, `email`, `created_at`, `updated_at`, `level_id`) VALUES
(1, 'admin', 'f865b53623b121fd34ee5426c792e5c33af8c227', 'admin@admin.com', '2020-01-08 18:58:47', '2020-01-08 18:58:47', 1),
(3, 'user', '95c946bf622ef93b0a211cd0fd028dfdfcf7e39e', 'user@user.com', '2020-01-09 03:59:12', '2020-01-09 03:59:12', 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `master_grade`
--
ALTER TABLE `master_grade`
  ADD PRIMARY KEY (`id_grade`);

--
-- Indeks untuk tabel `master_jenis_ikan`
--
ALTER TABLE `master_jenis_ikan`
  ADD PRIMARY KEY (`id_jenis_ikan`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `nelayan_level`
--
ALTER TABLE `nelayan_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indeks untuk tabel `nelayan_sub_tpi`
--
ALTER TABLE `nelayan_sub_tpi`
  ADD PRIMARY KEY (`id_sub_tpi`);

--
-- Indeks untuk tabel `nelayan_tpi`
--
ALTER TABLE `nelayan_tpi`
  ADD PRIMARY KEY (`id_tpi`);

--
-- Indeks untuk tabel `nelayan_transaksi`
--
ALTER TABLE `nelayan_transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indeks untuk tabel `nelayan_users`
--
ALTER TABLE `nelayan_users`
  ADD PRIMARY KEY (`id_users`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `master_grade`
--
ALTER TABLE `master_grade`
  MODIFY `id_grade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `master_jenis_ikan`
--
ALTER TABLE `master_jenis_ikan`
  MODIFY `id_jenis_ikan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `nelayan_level`
--
ALTER TABLE `nelayan_level`
  MODIFY `id_level` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `nelayan_sub_tpi`
--
ALTER TABLE `nelayan_sub_tpi`
  MODIFY `id_sub_tpi` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `nelayan_tpi`
--
ALTER TABLE `nelayan_tpi`
  MODIFY `id_tpi` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `nelayan_transaksi`
--
ALTER TABLE `nelayan_transaksi`
  MODIFY `id_transaksi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `nelayan_users`
--
ALTER TABLE `nelayan_users`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
