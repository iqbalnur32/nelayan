<?= view('src/layouts/header', ['title' => 'Dashboard Admin', 'error' => 'error']) ?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <?= view('src/layouts/title', ['title' => 'Admin Report']) ?>

  <!-- Content Row -->

  <div class="row" style="margin-left: 2px;">

    <!-- Earnings (Monthly) Card Example -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    	<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>

  </div>
</div>
<!-- /.container-fluid -->

<?= view('src/layouts/footer') ?>