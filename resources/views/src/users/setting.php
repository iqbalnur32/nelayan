<?= view('src/layouts/header', ['title' => 'Dashboard Admin', 'error' => 'error']) ?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <?= view('src/layouts/title', ['title' => 'Management User']) ?>

<form method="POST" enctype="multipart/form-data" action="<?= url('admin/management/edit/' . $data_edit->id_users) ?>">
  <input type="hidden" name="id_users" value="<?= $data_edit->id_users?>">
  <div class="row">
    <div class="card col-lg-6">
      <div class="card-heading">
        <div class="card-body">
          <h4> User Acccount </h4>
          <hr>
            <div class="form-group">
              <label>Username</label>
              <input name='username' type="text" class="form-control" required="" value="<?= $data_edit->username; ?>">
            </div>
            <div class="form-group">
              <label>Email</label>
              <input name="email" type="email" class="form-control" required="" value="<?= $data_edit->email; ?>">
            </div>
            <div class="form-group">
              <label>Password</label>
              <input name="password" type="password" class="form-control" required="" value="<?= $data_edit->password; ?>">
            </div>
        </div>
      </div>
    </div>
    <div class="card col-lg-6">
      <div class="card-heading">
        <div class="card-body">
          <h4> User Profile </h4>
          <hr>
          <div class="form-group">
            <label>Nama</label>
            <input name="nama" type="text" class="form-control" required="" value="<?= $data_edit->name; ?>">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea class="form-control"  name="alamat" required="" placeholder="Alamat..."><?= $data_edit->alamat; ?></textarea>
          </div>
          <div class="form-group">
            <label>No Telp</label>
            <input name="no_telp" onkeyup="this.value=this.value.replace(/[^\d]/,'')" maxlength="13" type="text" class="form-control" required="" value="<?= $data_edit->no_telp; ?>">
          </div>
          <div class="form-group">
            <label>Hak Akses</label>
            <select class="form-control" name="level_id">
              <?php foreach ($level as $key): ?>
                <?php if ($key->id_level === 1 ): ?>
                  <option value="<?= $key->id_level ?>"><?= $key->name ?></option>
                <?php endif ?>
              <?php endforeach ?> 
            </select>
          </div>
          <div class="form-group">
            <label>Foto</label>
            <input name="foto" type="file" class="form-control" required="">
            <br>
            <img src="<?= url('/admin/file/'.$data_edit->foto) ?>" style="width: 200px; height: 170px">
            <br>
          </div>
          <br>
          <button type="submit" class="btn btn-primary btn-xl btn-block">
            <i class="fa fa fa-save"></i> Update
          </button>
        </div>
      </div>
    </div>
  </div>
</form>

<?= view('src/layouts/footer') ?>