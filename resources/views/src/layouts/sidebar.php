    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center">
        <div class="sidebar-brand-icon rotate-n-15">
        </div>
        <div class="sidebar-brand-text mx-3" style="color: white;">Nelayan</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <br>
      <!-- Nav Item - Dashboard -->
      <!-- Divider -->

      <!-- Nav Item - Pages Collapse Menu -->
      <?php 
      if($_SESSION['level_id'] === 1){
        ?>
                <li class="nav-item active">
                  <a class="nav-link" href="<?= url('admin')?>">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                  </a>
                </li>
                 <li class="nav-item" >
                  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Master Data</span>
                  </a>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                     <!--  <h6 class="collapse-header" href="#">Master Data</h6> -->
                     <a class="collapse-item" href="<?= url('admin/jenis-ikan') ?>" >Jenis Ikan</a>
                     <a class="collapse-item" href="<?= url('admin/grade-ikan')?>">Grade Ikan</a>
                   </div>
                 </div>
               </li>
               <li class="nav-item">
                <a class="nav-link " href="<?= url('admin/management')?>" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                  <i class="fas fa fa-address-book"></i>
                  <span>Management User</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?= url('admin/setting') ?>" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                  <i class="fas fa-fw fa-user"></i>
                  <span>Setting</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?= url('admin/reporting') ?>" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                  <i class="fas fa-fw fa-file"></i>
                  <span>Reporting</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?= url('user/logout-admin')?>" data-toggle="modal" data-target="#logoutModal" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                  <i class="fas fa-fw fa-sign-out-alt "></i>
                  <span>Logout</span>
                </a>
              </li>
        <?php        
      
      }elseif($_SESSION['level_id'] === 2){
       ?>
             <li class="nav-item active">
                <a class="nav-link" href="<?= url('user')?>">
                  <i class="fas fa-fw fa-tachometer-alt"></i>
                  <span>Dashboard</span>
                </a>
              </li>
               <li class="nav-item">
                <a class="nav-link " href="<?= url('user/setting') ?>" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                  <i class="fas fa-fw fa-user"></i>
                  <span>Setting</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?= url('user/reporting') ?>" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                  <i class="fas fa-fw fa-file"></i>
                  <span>Reporting</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?= url('user/logout-user')?>" data-toggle="modal" data-target="#logoutModal" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                  <i class="fas fa-fw fa-sign-out-alt "></i>
                  <span>Logout</span>
                </a>
              </li>
      <?php    
      }
      ?>
      <!-- Nav Item - Utilities Collapse Menu -->
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>
    <!-- End of Sidebar -->