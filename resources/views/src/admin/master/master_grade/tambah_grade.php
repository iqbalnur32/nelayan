<?= view('src/layouts/header', ['title' => 'Dashboard Admin', 'error' => 'error']) ?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <?= view('src/layouts/title', ['title' => 'Dashboard Grade']) ?>
  <br><br>
  <div class="row">
    <form class="col-md-10" method="POST" action="<?= url('admin/grade-ikan/add') ?>">
        <div class="card col-md-10">
            <div class="card-heading">
                <div class="card-body">
                    <h4> Tambah Ikan </h4>
                    <hr>
                    <div class="form-group">
                      <label>Name</label>
                        <input name='name' type="text" class="form-control" name="" required="">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input name="description" type="text" class="form-control" name="" required="">
                    </div>
                    <div class="form-group">
                        <label>Berat-Min</label>
                        <input name="berat_min" type="text" class="form-control" name="" required="">
                    </div>
                    <div class="form-group">
                        <label>Berat-Max</label>
                        <input name="berat_max" type="text" class="form-control" name="" required="">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary btn-xl btn-block">
                      <i class="fa fa fa-plus"></i>
                        Tambah Ikan
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
</div>

<!-- Success Alert -->
<script type="text/javascript">
  <?php 
  if(isset($type)){
      ?>
      swal("<?= $title ?>", "<?= $msg ?>", "<?= $type ?>");
      setTimeout(function() { window.location = '<?= url("admin/grade-ikan") ?>' }, 3000);
      <?php
  }
  ?>
</script>
<?= view('src/layouts/footer') ?>