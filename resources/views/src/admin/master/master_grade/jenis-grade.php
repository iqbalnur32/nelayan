<?= view('src/layouts/header', ['title' => 'Dashboard Admin']) ?>

<!-- Begin Page Content -->
<div class="container-fluid">

  <?= view('src/layouts/title', ['title' => 'Grade Ikan']) ?>
  <!-- Form Jenis Grade -->
  <form class="user" method="GET" action="<?= url('admin/grade-ikan/add')?>">
    <div class="container">
      <div class="card mt-5 border-left-primary border-bottom-primary ">
        <!-- Body Jenis Grade -->
        <div class="card-body">
            <label>
                <button type="submit" class="btn btn-primary btn-block btn-sm" title="Tambah Data"> 
                <li class="fa fa fa-plus "></li> Tambah </button>
            </label>
            <br/><br>
          <!-- Table Grade -->
          <table class="table table-bordered table-hover  table-striped">
            <thead class="border-primary">
              <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Berat_Min</th>
                  <th>Berat_max</th>
                  <th>OPSI</th>
              </tr>
            </thead>
            <?php $i = 1; foreach ($data_ikan as $p): ?>
              <tbody>
                <tr>
                  <td class="text-capitalize text-dark"><?php echo $i++; ?></td>
                  <td class="text-capitalize text-dark"><?php echo $p->name; ?></td>
                  <td class="text-capitalize text-dark"><?php echo $p->description; ?></td>
                  <td class="text-capitalize text-dark"><?php echo $p->berat_min; ?></td>
                  <td class="text-capitalize text-dark"><?php echo $p->berat_max; ?></td>
                  <td>
                    <a class="btn btn-warning btn-sm " style="color: white;" href="<?= url('admin/grade-ikan/edit/grade/'. $p->id_grade)?>" >
                      <i class="fas fa-fw fa-pencil-alt"></i><span>Edit</span>
                    </a>
                    <a class="btn btn-danger btn-sm " style="color: white;" href="<?= url('admin/grade-ikan/hapus/grade/' . $p->id_grade)?>" >
                      <i class="fas fa-fw fa fa-trash"></i><span>Hapus</span>
                    </a>
                </tr>
              </tbody>
            <?php endforeach ?>
          </table>
        </div>
        <!-- End Body Grade -->
      </div>
    </div>
  </form>
  <!-- End From Jenis Grade -->
</div>
<!-- /.container-fluid -->

<?= view('src/layouts/footer') ?>
