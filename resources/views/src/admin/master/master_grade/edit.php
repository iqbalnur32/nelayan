<?= view('src/layouts/header', ['title' => 'Dashboard Admin', 'error' => 'error']) ?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <?= view('src/layouts/title', ['title' => 'Dashboard Grade']) ?>
  <br><br>
    <div class="row">
        <form class="col-md-10" method="POST" action="<?= url('admin/grade-ikan/edit/grade/' . $grade_ikan->id_grade) ?>">
            <div class="card col-md-10">
                <div class="card-heading">
                    <div class="card-body">
                        <h4> Edit Grade Ikan </h4>
                        <hr>
                        <div class="form-group">
                          <label>nama</label>
                            <input name='name' type="text" class="form-control"  required="" value="<?= $grade_ikan->name?>">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <input name="description" type="text" class="form-control" required="" value="<?= $grade_ikan->description?>">
                        </div>
                        <div class="form-group">
                            <label>Berat-Min</label>
                            <input name="berat_min" type="text" class="form-control" required="" value="<?= $grade_ikan->berat_min?>">
                        </div>
                        <div class="form-group">
                            <label>Berat-Max</label>
                            <input name="berat_max" type="text" class="form-control" required="" value="<?= $grade_ikan->berat_max?>">
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary btn-sm btn-block"><i class="fas fa-fw fa-pencil-alt"></i>Edit Grade
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?= view('src/layouts/footer') ?>