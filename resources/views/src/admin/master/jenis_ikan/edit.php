<?= view('src/layouts/header', ['title' => 'Dashboard Admin', 'error' => 'error']) ?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <?= view('src/layouts/title', ['title' => 'Edit Jenis Ikan']) ?>
 <br><br>
    <div class="row">
        <form class="col-md-10" method="POST" action="<?= url('admin/jenis-ikan/edit/jenis/'. $data_edit->id_jenis_ikan) ?>">
            <div class="card col-md-10">
                <div class="card-heading">
                    <div class="card-body">
                        <h4> Edit Ikan </h4>
                        <hr>
                        <div class="form-group">
                            <label>Nama</label>
                            <input name='name' type="text" class="form-control" value="<?= $data_edit->name?>">
                        </div>
                        <div class="description">
                            <label>Description</label>
                            <input name="description" type="text" class="form-control" value="<?= $data_edit->description?>">
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            <i class="fas fa-fw fa-pencil-alt"></i>Edit Ikan
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<?= view('src/layouts/footer') ?>