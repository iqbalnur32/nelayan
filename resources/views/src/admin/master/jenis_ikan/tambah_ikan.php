<?= view('src/layouts/header', ['title' => 'Dashboard Admin', 'error' => 'error']) ?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <?= view('src/layouts/title', ['title' => 'Jenis Ikan']) ?>
 <br>
  <div class="row">
    <form class="col-md-10" method="POST" action="<?= url('admin/jenis-ikan/add') ?>">
      <div class="card col-md-10">
        <div class="card-heading">
          <div class="card-body">
           <h4> Tambah Ikan </h4>
            <hr>
              <div class="form-group">
                <label>Nama</label>
                <input name='name' type="text" class="form-control" name="">
              </div>
              <div class="description">
                <label>Description</label>
                <input name="description" type="text" class="form-control" name="">
              </div>
              <br>
            <button type="submit" class="btn btn-primary btn-user btn-block"><i class="fa fa-fw fa-plus"></i>Tambah Ikan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- End Page Content -->

<!-- success alert -->
<script>
  <?php 
    if(isset($type)){
      ?>
        swal("<?= $title ?>", "<?= $msg ?>", "<?= $type ?>");
        setTimeout(function() { window.location = '<?= url("admin/jenis-ikan") ?>' }, 3000);
      <?php
    }
  ?>
</script>

<?= view('src/layouts/footer') ?>