<?= view('src/layouts/header', ['title' => 'Dashboard Admin']) ?>

<!-- Begin Page Content -->
<div class="container-fluid">

  <?= view('src/layouts/title', ['title' => 'Jenis Ikan']) ?>
  <form class="user" method="GET" action="<?= url('admin/jenis-ikan/add')?>">
    <!-- Container  -->
    <div class="container">
      <div class="card mt-5 border-left-primary border-bottom-primary ">
        <div class="card-body">
          <label>
            <button type="submit" class="btn btn-primary btn-block btn-sm " title="Tambah Data">
              <li class="fa fa fa-plus"></li>Tambah
            </button>
          </label>
          <!--Table  -->
          <table class="table table-bordered table-hover  table-striped">
            <thead class="border-primary">
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <!-- Looping Data Berdasakarkan Nama dan Description -->
              <?php $i = 1; foreach ($data as $p ): ?>
                <tr>
                  <td class="text-capitalize text-dark"><?php echo $i++; ?></td>
                  <td class="text-capitalize text-dark"><?php echo($p->name)?></td>
                  <td class="text-capitalize text-dark"><?php echo($p->description)?></td>
                  <td> 
                    <a class="btn btn-warning btn-sm " style="color: white;" href="<?= url('admin/jenis-ikan/edit/jenis/'. $p->id_jenis_ikan)?>" >
                      <i class="fas fa-fw fa-pencil-alt"></i>
                      <span>Edit</span>
                    </a>
                    <a class="btn btn-danger btn-sm " style="color: white;" href="<?= url('admin/jenis-ikan/hapus/jenis/' . $p->id_jenis_ikan)?>" >
                      <i class="fas fa fa-trash"></i>
                      <span>Hapus</span>
                    </a>
                  </td>
                </tr>
              <?php endforeach ?>
              <!-- nd Looping Data Berdasakarkan Nama dan Description -->
            </tbody>
          </table>
          <!-- End Table -->
        </div>
      </div>
    </div>
    <!-- End Container -->
  </form>
</div>
<!-- /.container-fluid -->

<?= view('src/layouts/footer') ?>
