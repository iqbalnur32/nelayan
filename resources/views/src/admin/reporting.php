<?= view('src/layouts/header', ['title' => 'Dashboard Admin', 'error' => 'error']) ?>
<!-- Begin Page Content -->
<div class="container-fluid">
	<?= view('src/layouts/title', ['title' => 'Reporting']) ?>
	<!-- Content Row -->
	<div class="row">
		<!-- Begin Page Content -->
		<div class="container-fluid">
			<form class="user" method="GET" action="<?= url('admin/reporting/cetak')?>">
				<div class="">
					<div class="card mt-6 border-left-primary border-bottom-primary ">
						<div class="card-body">
							<label>
							<button type="submit" class="btn btn-primary  btn-block" title="Tambah Data"><li class="fa fa-fw fa-plus"></li>Tambah</button>
							</label>
							<br/><br>
			
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.container-fluid -->
	</div>
</div>

<!-- /.container-fluid -->

<?= view('src/layouts/footer') ?>