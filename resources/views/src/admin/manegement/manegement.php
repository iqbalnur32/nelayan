<?= view('src/layouts/header', ['title' => 'Dashboard Admin', 'error' => 'error']) ?>
<!-- Begin Page Content -->
<div class="container-fluid">
	<?= view('src/layouts/title', ['title' => 'Management User']) ?>
	<!-- Content Row -->
	<div class="row">
		<!-- Begin Page Content -->
		<div class="container-fluid">
			<form class="user" method="GET" action="<?= url('admin/management/add')?>">
				<div class="">
					<div class="card mt-6 border-left-primary border-bottom-primary ">
						<div class="card-body">
							<label>
								<button type="submit" class="btn btn-primary  btn-block" title="Tambah Data"><li class="fa fa-fw fa-plus"></li>Tambah</button>
							</label>
							<br/><br>
							<table class="table table-bordered table-hover  table-striped">
								<thead class="border-primary">
									<tr>
										<th>No</th>
										<th>Username</th>
										<th>Email</th>
										<th>Alamat</th>
										<th>No Telp</th>
										<th>Tgl Input</th>
										<th>Hak Akses</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i = 1; foreach ($data_user as $user ): ?>
										<tr>
											<td class="text-capitalize text-dark"><?php echo $i++; ?></td>
											<td class="text-capitalize text-dark"><?php echo $user->username; ?></td>
											<td class="text-capitalize text-dark"><?php echo $user->email; ?></td>
											<td class="text-capitalize text-dark"><?php echo $user->alamat; ?></td>
											<td class="text-capitalize text-dark"><?php echo $user->no_telp; ?></td>
											<td class="text-capitalize text-dark"><?php echo substr($user->tanggal, 0,10); ?></td>
											<td class="text-capitalize text-dark"><?php echo $user->name; ?></td>
											<td>
												<a class="btn btn-warning btn-sm " href="<?= url('admin/management/edit/' . $user->id_users)?>" >
													<i class="fas fa-fw fa-pencil-alt"></i>
												</a>
												<a class="btn btn-danger btn-sm " href="<?= url('admin/management/hapus/'. $user->id_users)?>" >
													<i class="fas fa-fw fa fa-trash"></i>
												</a>
											</td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.container-fluid -->
	</div>
</div>

<!-- /.container-fluid -->

<?= view('src/layouts/footer') ?>