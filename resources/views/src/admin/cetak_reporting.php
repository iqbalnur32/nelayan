<?php 

header('Content-Type: application/pdf');
?>
		<html>
		<head>
		<title>Reporting</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		</head>
		<body>
		<style type="text/css">
			table tr td,
			table tr th{
				font-size: 9pt;
			}
		</style>
		<center>
			<h5>Data Laporan</h4>
			</center>

			<table class='table table-bordered'>
				<thead>
					<tr>
						<th>No</th>
						<th>Nama</th>
						<th>Email</th>
						<th>Alamat</th>
						<th>Telepon</th>
						<th>Tanggal</th>
						<th>Name</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach ($cetak_reporting as $user ): ?>
					<tr>
						<td class="text-capitalize text-dark"><?php echo $i++; ?></td>
						<td class="text-capitalize text-dark"><?php echo $user->username; ?></td>
						<td class="text-capitalize text-dark"><?php echo $user->email; ?></td>
						<td class="text-capitalize text-dark"><?php echo $user->alamat; ?></td>
						<td class="text-capitalize text-dark"><?php echo $user->no_telp; ?></td>
						<td class="text-capitalize text-dark"><?php echo substr($user->tanggal, 0,10); ?></td>
						<td class="text-capitalize text-dark"><?php echo $user->name; ?></td>
						<td>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>

		</body>
		</html>

<?php 

?>

